# nb_heart_failure
# Bence Nemeth, n.bence4@gmail.com
# 21.08.2019
# classification of heart failure

### SOURCES ###
Github repository:
git@gitlab.com:n.bence4/nb_heart_failure.git

Dabase and csv files have been uploaded to:
https://drive.google.com/drive/folders/1GtY6g0uvHjMsBTdm_10GM437WA0a9pwD?usp=sharing


Documentation of processing can be read here:
https://docs.google.com/document/d/1d2l92RsdmhNJRGFZwqmZqrxWcEXHZiVmN8f2-9zNaY4/edit#heading=h.g19345gy64jl

### FILE STRUCTURE ###
─────────── original
│       ├── atc_code_table.csv
│       ├── data_science_task_datasets.zip
│       ├── demography.csv
│       ├── hospitalization_icd.csv
│       ├── hospitalization_icpm.csv
│       ├── icd_code_table.csv
│       ├── icpm_code_table.csv
│       ├── outpatient_icd.csv
│       ├── outpatient_icpm.csv
│       └── prescriptions.csv
├── data_assesment_task_spicy.docx
├── data_exploration.ipynb
├── preprocessing.ipynb
├── README.md
├── requirements.txt
├── train_benchmark.py
└── train.py


### 
