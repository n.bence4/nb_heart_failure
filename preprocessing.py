
# coding: utf-8


# notes:
# https://docs.google.com/document/d/1d2l92RsdmhNJRGFZwqmZqrxWcEXHZiVmN8f2-9zNaY4/edit#heading=h.g19345gy64jl


import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
from math import isnan


import sqlite3
from sqlalchemy import create_engine
import os



DIR = os.getcwd() + '/data/'


# read the 6 tables with data
df_demography = pd.read_csv(os.getcwd() + "/data/demography.csv", keep_default_na=False, na_values=[""]) #  assign empty values in our CSV to NaN
df_hosp_icd = pd.read_csv(os.getcwd() + "/data/hospitalization_icd.csv", keep_default_na=False, na_values=[""])
df_hosp_icpm = pd.read_csv(os.getcwd() + "/data/hospitalization_icpm.csv", keep_default_na=False, na_values=[""])
df_outp_icd = pd.read_csv(os.getcwd() + "/data/outpatient_icd.csv", keep_default_na=False, na_values=[""])
df_outp_icpm = pd.read_csv(os.getcwd() + "/data/outpatient_icpm.csv", keep_default_na=False, na_values=[""])
df_prescriptions = pd.read_csv(os.getcwd() + "/data/prescriptions.csv", keep_default_na=False, na_values=[""])

csv_database = create_engine('sqlite:///csv_database.db')


df_demography.to_sql('demography',csv_database, if_exists='replace',index = False)
df_hosp_icd.to_sql('hosp_icd', csv_database,if_exists='replace', index = False)
df_hosp_icpm.to_sql('hosp_icpm', csv_database,if_exists='replace', index = False)
df_outp_icd.to_sql('outp_icd', csv_database,if_exists='replace',index = False)
df_outp_icpm.to_sql('outp_icpm',csv_database,if_exists='replace', index = False)
df_prescriptions.to_sql('prescriptions',csv_database,if_exists='replace', index = False)


# DATATABLES: 
# DEMOGRAPHY : AGE, I48
# ICD: DIAGNOSIS
# ICPM: THERAPY
# PRESCRIPTION: GIVEN DRUGS
# + 3 dictionaries

# ICD ICD-10 is the 10th revision of the International Statistical Classification of Diseases and Related Health Problems (ICD), a medical classification list by the World Health Organization (WHO).
# It contains all the patient diagnosis codes recorded in a hospital treatment. The ID column can be used to join with the other tables, and each row is a single event represented by its ICD code.
# the ICPM codes in this table records every given therapy in a hospital. 
# The International Classification of Procedures in Medicine (ICPM) was a system of classifying procedure 
# prescriptions.csv: It mainly shows all the ATC codes which relate to each patients filled prescriptions. The ATC system codes the medicine agents by molecules and these are important features to evaluate the heart failure risk. This table contains addition ICD information as the target disease indicated on the prescriptions.

# statistics

# age distribution of patients
df_demography['AGE']
del df_hosp_icd, df_hosp_icpm, df_outp_icd, df_outp_icpm, df_prescriptions

## Task

# Please build a classifier to predict the risk of heart failure (I50) in the first year after the I48 diagnosis. The input features of the classification model can be anything that happened BEFORE the first I48 diagnosis and demographic features. Please evaluate your classifier’s results.

# we are interested in those patients who had I50* heart failure
# in 1 year after diagnosis of i48
# matches for i50 pattern: I501,I502,I5020,I5021,I5022,I5023,I503,I5030,I5031,I5032,I5033,I504,I5040,I5041,I5042,I5043,I508,I5081,I50810,I50811,I50812,I50813,I50814,I5082,I5083,I5084,I5089,I509

# choose the patients who got I50 in hospital in 1 year after  diagnosis of I48
query_i50 = """ SELECT demography.ID, demography.DIAGNOSIS_DATE_OF_I48, hosp_icd.ICD, hosp_icd.DATE
FROM demography LEFT JOIN hosp_icd
ON demography.ID = hosp_icd.ID
WHERE hosp_icd.ICD LIKE 'I50%'
AND julianday(demography.DIAGNOSIS_DATE_OF_I48) > julianday(hosp_icd.DATE,'-1 years');
""" 
df_hospi50 = pd.read_sql_query(query_i50, csv_database)


# set with all the ID-s who will get "positive" flag
set_positives = set(df_hospi50['ID'])

# add class label to demography column
df_demography['CLASS'] = df_demography['ID'].isin(set_positives)

# update demography table in database
df_demography.to_sql('demography',csv_database, if_exists='replace',index = False)

df_final = df_demography

df_final.drop(columns=['DIAGNOSIS_DATE_OF_I48','DECEASED_DATE'], inplace = True)


query_prescr = """
SELECT demography.ID, prescriptions.ATC
FROM demography LEFT JOIN prescriptions
ON demography.ID = prescriptions.ID
WHERE julianday(demography.DIAGNOSIS_DATE_OF_I48) > julianday(prescriptions.DATE)
;
"""
df_prescriptions = pd.read_sql_query(query_prescr, csv_database)


query_prescriptions = """SELECT d.ID, d.DIAGNOSIS_DATE_OF_I48,p.DATE, p.ATC
FROM demography d LEFT JOIN prescriptions p
ON d.ID = p.ID
WHERE julianday(d.DIAGNOSIS_DATE_OF_I48) > julianday(p.DATE)"""

df_precriptions_filtered= pd.read_sql_query(query_prescriptions, csv_database)

query_hosp_dcm = """SELECT d.ID, hd.ICD
FROM demography d LEFT JOIN hosp_icd hd
ON d.ID = hd.ID
WHERE julianday(d.DIAGNOSIS_DATE_OF_I48) > julianday(hd.DATE)"""

df_hosp_dcm_filtered= pd.read_sql_query(query_hosp_dcm, csv_database)

query_outp_icpm = """SELECT d.ID, opm.ICD
FROM demography d LEFT JOIN outp_icd opm
ON d.ID = opm.ID
WHERE julianday(d.DIAGNOSIS_DATE_OF_I48) > julianday(opm.DATE)"""

df_outp_dcm_filtered= pd.read_sql_query(query_outp_icpm, csv_database)


query_outp_icpm = """SELECT d.ID, od.icpm
FROM demography d LEFT JOIN outp_icpm od
ON d.ID = od.ID
WHERE julianday(d.DIAGNOSIS_DATE_OF_I48) > julianday(od.DATE)"""

df_outp_icpm_filtered= pd.read_sql_query(query_outp_icpm, csv_database)

query_hosp_icpm = """SELECT d.ID, hpm.icpm
FROM demography d LEFT JOIN outp_icpm hpm
ON d.ID = hpm.ID
WHERE julianday(d.DIAGNOSIS_DATE_OF_I48) > julianday(hpm.DATE)"""

df_hosp_icpm_filtered= pd.read_sql_query(query_hosp_icpm, csv_database)


# ### Create sets ###


# aggregate columns
# collect all unique values along ATC values
set_prescr= df_precriptions_filtered[['ID', 'ATC']].groupby('ID', as_index=False).agg(lambda x: set(x))

# concatenate outpatient and hospital frames
# ICD codes will be handled together (hospital and outpatient) during the machine learning classifier training
df_icds = pd.concat([df_hosp_dcm_filtered, df_outp_dcm_filtered])
print(len(df_hosp_dcm_filtered), len(df_outp_dcm_filtered), len(df_icds))
assert len(df_hosp_dcm_filtered)+ len(df_outp_dcm_filtered) == len(df_icds)
set_icds = df_icds.groupby('ID', as_index=False).agg(lambda x: set(x))


# concatenate outpatient and hospital frames
# ICPM codes will be handled together (hospital and outpatient) during the machine learning classifier training
df_icpms = pd.concat([df_hosp_icpm_filtered, df_outp_icpm_filtered])
print(len(df_hosp_icpm_filtered), len(df_outp_icpm_filtered), len(df_icpms))
assert len(df_hosp_icpm_filtered)+ len(df_outp_icpm_filtered) == len(df_icpms)
set_icpms = df_icpms.groupby('ID', as_index=False).agg(lambda x: set(x))

# ### Add new feature columns ###

# left join prescription on ID
df_final = pd.merge(df_final, set_prescr, how='left', on='ID') # aggreagate to set of prescriptions


df_final = pd.merge(df_final, set_icds, how='left', on='ID') # add set of icds as a new feature

df_final = pd.merge(df_final, set_icpms, how='left', on='ID') # add set of icpms as a new feature


# ### Process  codes as sparse matrix ###

df_atc_codes = pd.read_csv(os.getcwd() + "/data/atc_code_table.csv", keep_default_na=False,  header=None,na_values=[""])
df_icd_codes = pd.read_csv(os.getcwd() + "/data/icd_code_table.csv", keep_default_na=False,  header=None,na_values=[""])
df_icpm_codes = pd.read_csv(os.getcwd() + "/data/icpm_code_table.csv", keep_default_na=False,  header=None, na_values=[""])


## process ATC codes ##

num_atc = len(df_atc_codes)
num_icd = len(df_icd_codes)
num_icpm = len(df_icpm_codes)
num_patients = len(df_final)
print('Number of atc codes {}, icd {}, icpm {}, patients {}'.format(num_atc, num_icd, num_icpm, num_patients))

# collect the unique elements from ACT codes
all_atc_codes = df_atc_codes[0]
all_sets_atc = df_final['ATC']
list_sets_atc = [i_set[1]  for i_set in all_sets_atc.iteritems() if not pd.isna(i_set) and type(i_set[1]) is set]
set_acts = set.union(*list_sets_atc) # unique set of atc codes present in dataset
del list_sets_atc
print('Number of all acts codes: {} \nnumber of acts codes present in dataset: {}'.format(len(all_atc_codes), len(set_acts)))

# collect the unique elements from ICD codes
all_icd_codes = df_icd_codes[0]
all_sets_icd = df_final['ICD']
list_sets_icd = [i_set[1]  for i_set in all_sets_icd.iteritems() if not pd.isna(i_set) and type(i_set[1]) is set]
set_icds = set.union(*list_sets_icd)
del list_sets_icd
print('Number of all icd codes: {} \nnumber of icd codes present in dataset: {}'.format(len(all_icd_codes), len(set_icds)))

# collect the unique elements from ICPM codes
all_icpm_codes = df_icpm_codes[0]
all_sets_icpm = df_final['ICPM']
list_sets_icpm = [i_set[1]  for i_set in all_sets_icpm.iteritems() if not pd.isna(i_set) and type(i_set[1]) is set]
set_icpms = set.union(*list_sets_icpm)
del list_sets_icpm
print('Number of all icpm codes: {} \nnumber of icpm codes present in dataset: {}'.format(len(all_icpm_codes), len(set_icpms)))


# initialize feature table with boolean values
temp = np.empty((num_patients,len(set_acts)), dtype=bool)
temp.fill(False) # initialize with false values
df_atc = pd.DataFrame(index= df_final.index, columns=set_acts, data = temp, dtype = bool )
#df_atc = df_atc.fillna(False)


#import pymp
# iterate through patients
df_final_ATC = df_final['ATC']
#for i_atc in range(len(df_final)):
#for i_patient, _ in enumerate(range(20)):

#with pymp.Parallel(4) as p:
#for i_patient in range(200):

#for i_patient in p.range(len(df_atc)):
for i_patient, _ in enumerate(range(len(df_atc))):

    #iterate through ATC codes of a patient
    set_acts_of_patient = (df_final_ATC[i_patient])
    if i_patient%100 == 0:
        print("Patient {}".format(i_patient))
        print(set_acts_of_patient)

    if not pd.isna(set_acts_of_patient):
        #counter = lambda x : df_atc.loc[i_patient, x] = True
        #map(counter, set_acts_of_patient)     

        df_atc.loc[i_patient, set_acts_of_patient] = True
        

df_atc.to_csv(path_or_buf= '/home/bence/projects/spicyanalitcs/data/atc_onehot.csv',index=False)

"""
icd codes  as boolean features
"""
temp = np.empty((num_patients,len(set_icds)), dtype=bool)
temp.fill(False) # initialize with false values
df_icd = pd.DataFrame(index= df_final.index, columns=set_icds, data = temp, dtype = bool )


df_final_icd = df_final['ICD']

for i_patient, _ in enumerate(range(len(df_icd))):

    set_acts_of_patient = (df_final_icd[i_patient])
    if i_patient%100 == 0:
        print("Patient {}".format(i_patient))
        print(set_acts_of_patient)

    if not pd.isna(set_acts_of_patient):
        df_icd.loc[i_patient, set_acts_of_patient] = True


#save
df_icd.to_csv(path_or_buf= '/home/bence/projects/spicyanalitcs/data/icd_onehot.csv',index=False)


"""
icpm codes as boolean features
"""
temp = np.empty((num_patients,len(set_icpms)), dtype=bool)
temp.fill(False) # initialize with false values
df_icpm = pd.DataFrame(index= df_final.index, columns=set_icpms, data = temp, dtype = bool )

df_final_icpm = df_final['ICPM']

for i_patient, _ in enumerate(range(len(df_icpm))):

    set_acts_of_patient = (df_final_icpm[i_patient])
    if i_patient%100 == 0:
        print("Patient {}".format(i_patient))
        print(set_acts_of_patient)

    if not pd.isna(set_acts_of_patient):
        df_icpm.loc[i_patient, set_acts_of_patient] = True


#save
df_icpm.to_csv(path_or_buf= '/home/bence/projects/spicyanalitcs/data/icpm_onehot.csv',index=False)


df_atc.memory_usage().sum()


